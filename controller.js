/**
 * Class: Controller
 *
 * @package Controller
 *
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
define('_controller', ['view', 'model', 'event', 'dic', 'option', 'require', 'configCore'], 
  function(View, Model, Event, Dic, Option, require, config){

  function _controller(){

  }

  _controller.prototype._event            = 'base/controller';
  _controller.prototype._template         = '';
  _controller.prototype._view             = null;
  _controller.prototype._params           = null;
  _controller.prototype.request           = null;
  _controller.prototype.stream            = null;

  _controller.prototype._before           = function(){
    return this.finalize();
  };
  _controller.prototype.finalize          = function(){};
  _controller.prototype._after            = function(){
    return this.initialize();  
  };
  _controller.prototype.initialize        = function(){};
  _controller.prototype._action           = function(){
    return this.action();
  };
  _controller.prototype.action            = function(){};

  _controller.prototype._run              = function(params, request, view){
    var template                          = null,
        _this                             = this,
        temp                              = null,
        after                             = function(){
          _this._after();
          if((_this._view instanceof View.constructor) && null != template){
            _this._view.set_filename(template);
          }
          return _this;
        };
    if (('undefined' != typeof view) && (view instanceof View.constructor)){
      this._view                          = view;
      template                            = view.get_filename();
      this._view.set_filename(this._template);
    }
    else{
      this._view                          = View.factory(this._template);
    }
    this.stream                           = new Option();
    this.request                          = new Option();
    if('undefined' === typeof request){
      Model.factory('request').then(function(request){
        _this.request.update({done:true, detail:request});
      });
    }
    else{
      this.request.update({done:true, detail:request});
    }

    return this.request.then(function(request){
        var uri                           = request.get('VAR_REQUEST_URI');
        if('undefined' === typeof params){
          params                          = new Array;
        }
        request.set('VAR_PARAMETERS', params);
        Event.trigger(_this._event, _this._view, _this);
        if(uri){
          if(uri instanceof Array){
            uri                           = uri.join('/');
          }
          Event.trigger(uri, _this._view, _this);
        }
      })
      .then(function(){
        var hadled;
        _this._before();
        _this.stream.update({done:true, detail:_this});
        handled                           = _this.stream.then().nocommit();
        return handled;
      })
      .then(after, after)
      .when(function(ctrl, resolve, reject){
        ctrl.stream.commit().then(resolve, reject);
      });
  };

  _controller.prototype._ensure           = function(err, stack){
    if(err){
      var err                             = new Error;
      err.stack                           = stack;
      throw err;
    }
  };

  _controller.prototype._sanitize_event   = function(event){
    return event.replace(/[\/\\]/g, '_').toLowerCase();
  };

  _controller.prototype.register_model    = function(value){
    this.ensure('string' !== typeof value, "Parameter '" + value + "' is not correct string model name.");
    value                                 = Model._sanitize_model(value);
    var mname                             = 'models/' + value,
        args                              = Array.prototype.slice.call(arguments);
    args[0]                               = mname;
    if(!Dic.isset(mname)){
      var minst                           = Dic.reuse(function(){
        var args                          = Array.prototype.slice.call(arguments);
        return Model.factory(value).then(function(model){
          model.capture.apply(model, args);
          return model;
        });
      });
      Dic.add(mname, minst);
    }
    return Dic.callService.apply(Dic, args);
  };

  _controller.prototype.ensure            = function(err, stack){
    if(err){
      var err                             = new Error;
      err.stack                           = stack;
      throw err;
    }
  };

  _controller.prototype.commit            = function(){
    return this.stream.commit();
  };

  _controller.prototype.factory           = function(child_name){
    var key_name                          = '_controller/' + (0 < this._event.length ? this._event + '/' : '') + child_name.toLowerCase(),
        ancestor                          = this,
        result                            = new Option(),
        src;
    function do_factory(src){
      Dic.add(key_name, function(){
        var func                          = {},
            ret                           = function(){},
            ctrl                          = null;
        func[ancestor._event]             = function(){};
        func[ancestor._event].prototype   = ancestor;
        ret.prototype                     = new func[ancestor._event];
        ret.prototype.ancestor            = ancestor;
        ret.prototype.constructor         = ancestor.constructor;
        ret.prototype._event              = ancestor._sanitize_event(child_name);
        ret.prototype._view               = null;
        ret.prototype._params             = null;
        ret.prototype._template           = '';
        ret.prototype.request             = null;
        ret.prototype.stream              = null;
        src(ret);
        ctrl                              = new ret;
        ancestor._ensure(!(ctrl instanceof ancestor.constructor), "The class <" + child_name + "> aren't implements <" + ancestor.constructor.name + "> class.");
        ancestor._ensure(!(ctrl instanceof _controller), 'Error format controller <' + child_name + '>');
        return ctrl;
      });
      result.update({
        done                            : true,
        detail                          : Dic.get(key_name)
      });
    }
    if(!Dic.isset(key_name)){
      if(-1 !== config.controllers.excludeSearch.indexOf(child_name)){
        do_factory(function(){});
      }
      else if(-1 !== config.controllers.core.indexOf(child_name)){
        require(['controllers/' + child_name], do_factory, function(err){
          do_factory(function(){});
        });
      }
      else{
        require(['app/controllers/' + child_name], do_factory, function(err){
          require(['controllers/' + child_name], do_factory, function(err){
            do_factory(function(){});
          });
        });
      }
      return result;
    }
    result.update({
      done                              : true,
      detail                            : Dic.get(key_name)
    });
    return result;
  };

  _controller.prototype.execute           = function(name, params, request){
    var result                            = new Option();
    this.factory(name).then(function(ctrl){
      ctrl._run(params, request).join(result);
    });
    return result;
  };

  _controller.prototype.extend            = function(name, view, params, request){
    var result                            = new Option();
    if (!view){
      view = this._view;
    }
    this.factory(name).then(function(ctrl){
      ctrl._run(params, request, view).join(result);
    });
    return result;
  };

  return _controller;
});

define('controller', ['_controller'], function(_controller){
  return new _controller;
});

