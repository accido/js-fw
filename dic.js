define('dic', function(){
  function _dic(){
  }

  _dic.prototype._vars                    = new Array;

  _dic.prototype._singletons              = new Array;

  _dic.prototype.isset                    = function(key){
    return ('undefined' !==  typeof this._vars[key]);
  };

  _dic.prototype.ensure                   = function(err, stack){
    if(err){
      var err                             = new Error;
      err.stack                           = stack;
      throw err;
    }
  };

  _dic.prototype.get                      = function(key){
    var param                             = this._vars[key];
    this.ensure('undefined' === typeof param, "Dependency '" + key + "' not defined.");
    return ('function' === typeof param ? param(this) : param);
  };

  _dic.prototype.set                      = function(key, value){
    this._vars[key]                        = value;
    return value;
  };

  _dic.prototype.callService              = function(){
    var args                              = Array.prototype.slice.call(arguments),
        key                               = args[0],
        param                             = this._vars[key];
    args[0]                               = this;
    this.ensure('undefined' === typeof param, "Dependency '" + key + "' not defined.");
    this.ensure('function' !== typeof param, "Dependency is not constructable: '" + key + "'");
    return param.apply(this, args);
  };

  _dic.prototype.add                      = function(key, value){
    if ('undefined' === typeof this._vars[key]){
      this._vars[key]                     = value;
      return true;
    }
    else{
      return false;
    }
  };

  _dic.prototype.unset                    = function(key){
    if('undefined' !== typeof this._vars[key]){
      delete this._vars[key];
      return true;
    }
    else{
      return false;
    }
  };

  _dic.prototype.reuse                    = function(callback){
    this.ensure('function' !== typeof callback, "Dependency is not constructable: '" + callback + "'");
    var container                         = this;
    return function(injector){
      var cs                              = container._singletons,
          instance                        = false, 
          args                            = Array.prototype.slice.call(arguments),
          alen                            = arguments.length,
          match                           = false,
          current                         = false,
          temp                            = null;
      for(var i in cs){
        if(cs[i].callback === callback){
          current                         = cs[i];
          for(var j=0, len=current.instances.length; j<len; ++j){
            match                         = true;
            temp                          = current.instances[j];
            if(alen !== temp.args.length){
              continue;
            }
            for(var k=0; k<alen; ++k){
              if(arguments[k] !== temp.args[k]){
                match                     = false;
                break;
              }
            }
            if(match){
              return temp.instance;
            }
          }
          break;
        }
      }
      instance                          = callback.apply(callback, args);
      if(false === current){
        current                         = {
          instances                     : [],
          'callback'                    : callback
        };
        cs[cs.length]                   = current;
      }
      temp                              = current.instances.length;
      current.instances[temp]           = {
        'instance'                      : instance,
        'args'                          : args
      }
      return instance;
    };
  };

  _dic.prototype.wrap                     = function(callback){
    return function(){
      return callback;
    }
  };

  _dic.prototype.get_closure              = function(key){
    var param                             = this._vars[key];
    this.ensure('undefined' === typeof param, "Dependency '" + key + "' not defined.");
    this.ensure('function' !== typeof param, "Dependency is not constructable: '" + key + "'");
    return param;
  };

  return new _dic;
});

