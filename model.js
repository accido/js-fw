/**
 * Class: _model
 *
 * @package _model
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
define('_model', ['require', 'option', 'event',  'dic', 'stream', 'configCore'],
  function(require, Option, Event, Dic, Stream, config){

  function _model(){

  };

  _model.prototype.vars                     = {VAR_EVENTS: {} };
  _model.prototype._classname               = 'Model';
  _model.prototype._streams                 = {};
  _model.prototype.done                     = function(){};
  _model.prototype.fail                     = function(){};

  _model.prototype._prepare                 = function(key){
    if('undefined' === typeof this._streams[key]){
      this._streams[key]                    = new Stream(this, key);
    }
  };

  _model.prototype._sanitize_model          = function(name){
    return name.replace(/[\/\\_]+/g, '/').replace(/^\/|\/$/g, '').toLowerCase();
  };

  _model.prototype._set                     = function (vars, key, value){
    var offset                              = key.split('.');
    if(1 === offset.length){
      vars[offset[0]]                       = value;
      return;
    }
    if('undefined' == typeof vars[offset[0]]){
      vars[offset[0]]                       = {};
    }
    this._set(vars[offset[0]], offset.slice(1).join('.'), value);
  };

  _model.prototype._get                     = function (vars, key){
    var offset                              = key.split('.');
    if('undefined' == typeof vars[offset[0]]){
      return undefined;
    }
    if(1 === offset.length){
      return vars[offset[0]];
    }
    return this._get(vars[offset[0]], offset.slice(1).join('.'));
  }

  _model.prototype._isset                   = function(vars, key){
    var offset                              = key.split('.');
    if('undefined' == typeof vars[offset[0]]){
      return false;
    }
    if(1 === offset.length){
      return true;
    }
    return this._isset(vars[offset[0]], offset.slice(1).join('.'));
  }

  _model.prototype._unset                   = function(vars, key){
    var offset                              = key.split('.');
    if('undefined' == typeof vars[offset[0]]){
      return false;
    }
    if(1 === offset.length){
      delete vars[offset[0]];
      return true;
    }
    this._unset(vars[offset[0]]. offset.slice(1).join('.'));
  }

  _model.prototype.set                      = function(key, value){
    this.ensure(!this.isset(key), "Dependency '" + key + "' not defined.");
    this._prepare(key);
    this._set(this.vars, key, value);
    this.trigger(key);
  };

  _model.prototype.trigger                  = function(key){
    this.ensure(!this.isset(key), "Dependency '" + key + "' not defined.");
    this._prepare(key);
    this._streams[key].update({
      done                                  : true,
      detail                                : this.get(key)
    });
  };

  _model.prototype.add                      = function(key, value){
    if(this.isset(key)) return false;
    this._prepare(key);
    this._set(this.vars, key, value);
    return true;
  };

  _model.prototype.get                      = function (key){
    this.ensure(!this.isset(key), "Dependency '" + key + "' not defined.");
    var param                               = this._get(this.vars, key);
    return ('function' === typeof param ? param.call(this, this) : param);
  };

  _model.prototype.register_param           = _model.prototype.add;

  _model.prototype.callService              = function(){
    var args                                = Array.prototype.slice.call(arguments),
        key                                 = args[0],
        param                               = null;
    args[0]                                 = this;
    this.ensure(!this.isset(key), "Dependency '" + key + "' not defined.");
    param                                   = this._get(this.vars, key);
    this.ensure('function' !== typeof param, "Dependency is not constructable: '" + key + "'");
    return param.apply(this, args);
  };

  _model.prototype.capture                  = function(injector){
    return null;
  };

  _model.prototype.instance                 = function(value){
    this.ensure('string' !== typeof value, "Parameter '" + value + "' is not correct string model name.");
    value                                   = this._sanitize_model(value);
    var mname                               = 'instance/models/' + value,
        args                                = Array.prototype.slice.call(arguments);
    args[0]                                 = mname;
    if(!Dic.isset(mname)){
      var minst                             = function(){
        var args                            = Array.prototype.slice.call(arguments);
        return _model.prototype.factory(value).then(function(model){
          model.capture.apply(model, args);
          return model;
        });
      };
      Dic.add(mname, minst);
    }
    return Dic.callService.apply(Dic, args);
  };

  _model.prototype.register_model           = function(value){
    this.ensure('string' !== typeof value, "Parameter '" + value + "' is not correct string model name.");
    value                                   = this._sanitize_model(value);
    var mname                               = 'models/' + value,
        args                                = Array.prototype.slice.call(arguments);
    args[0]                                 = mname;
    if(!Dic.isset(mname)){
      var minst                             = this.reuse(function(){
        var args                            = Array.prototype.slice.call(arguments);
        return _model.prototype.factory(value).then(function(model){
          model.capture.apply(model, args);
          return model;
        });
      });
      Dic.add(mname, minst);
    }
    return Dic.callService.apply(Dic, args);
  };

  _model.prototype.register                 = _model.prototype.register_model;

  _model.prototype.register_submodel        = function(value){
    this.ensure('string' !== typeof value, "Parameter '" + value + "' is not correct string model name.");
    value                                   = this._sanitize_model(value);
    var locale                              = 'models/' + value,
        args                                = Array.prototype.slice.call(arguments);
    args[0]                                 = locale;
    if(!this.isset(locale)){
      var minst                             = this.reuse(function(){
        var args                            = Array.prototype.slice.call(arguments);
        return _model.prototype.factory(value).then(function(model){
          model.capture.apply(model, args);
          return model;
        });
      });
      this.add(locale, minst);
    }
    return this.callService.apply(this, args);
  };

  _model.prototype.module                   = _model.prototype.register_submodel;

  _model.prototype._sanitize_path           = function(path){
    return path.replace(/[\/_\\]+/g,'_').replace(/^_|_$/g,'');
  };

  _model.prototype.handle                   = function(path, prioritet){
    var result                              = new Option(),
        func                                = function(view, ctrl){
          var prms;
          prms = result.commit()
          .promise(function(find, resolve, reject){
            if(find === ctrl){
              resolve(find);
              prms.off();
            }
          })
          .nocommit();
          ctrl.stream.listen(prms);
          result.update({done: true, detail: ctrl});
        },
        name;
    name                                    = this._sanitize_path(path);
    path                                    = path.replace(/\//g, '\/');
    if('undefined' == typeof prioritet){
      prioritet                             = Event.normal_priority;
    }
    Event.bind(path, func, _model.prototype, prioritet);
    Event.bind(name, func, _model.prototype, prioritet);
    return result;
  };

  _model.prototype.invoke                   = function(done, fail){
    if('function' == typeof done)
      this['done']                          = done;
    if('function' == typeof fail)
      this['fail']                          = fail;
  };

  _model.prototype.reuse                    = function(callback){
    return Dic.reuse(callback);
  };

  _model.prototype.wrap                     = function(callback){
    return Dic.wrap(callback);
  };

  _model.prototype.get_closure              = function(key){
    this.ensure(!this.isset(key), "Dependency '" + key + "' not defined.");
    var param                               = this._get(this.vars, key);
    this.ensure('function' !== typeof param, "Dependency is not constructable: '" + key + "'");
    return param;
  };

  _model.prototype.isset                    = function(key){
    return this._isset(this.vars, key)
  };

  _model.prototype.unset                    = function(key){
    return this._unset(this.vars, key);
  };

  _model.prototype.sleep                    = function(key){
    this.ensure(!this.isset(key), "Dependency '" + key + "' not defined.");
    this._prepare(key);
    this._set(this.vars, key, null);
    this._streams[key].destroy();
  };

  _model.prototype.promise                  = function(key){
    this.ensure(!this.isset(key), "Dependency '" + key + "' not defined.");
    this._prepare(key);
    return this._streams[key].promise(function(data, resolve, reject){
      resolve(data);
    });
  };

  _model.prototype.asEventStream            = function(key){
    this.ensure(!this.isset(key), "Dependency '" + key + "' not defined.");
    this._prepare(key);
    return this._streams[key];
  };

  _model.prototype.asPromiseStream          = _model.prototype.asEventStream;

  _model.prototype.asVipStream              = function(key){
    this.ensure(!this.isset(key), "Dependency '" + key + "' not defined.");
    this._prepare(key);
    return this.asPromiseStream(key).queue();
  };

  _model.prototype.regexp_escape            = function(s) {
    return String(s).replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g, '\\$1')
      . replace(/\x08/g, '\\x08');
  };

  _model.prototype.ensure                   = function(err, stack){
    if(err){
      var err                               = new Error;
      err.stack                             = stack;
      throw err;
    }
  };

  _model.prototype.exception_ensure         = function(){
    var callback                            = Array.prototype.shift.call(arguments);
    var context                             = Array.prototype.shift.call(arguments);
    try{
      return callback.apply(context, arguments);
    }
    catch(e){
      var ModelError                        = new Function,modelError;
      ModelError.prototype                  = e;
      modelError                            = new ModelError;
      modelError._class                     = context._classname;
      modelError._function                  = callback;
      modelError.context                    = context;
      modelError.arguments                  = arguments;
      throw modelError;
    }
  }

  _model.prototype.bind_events              = function(event){
    if (this.isset('VAR_EVENTS')){
      var es = this.get('VAR_EVENTS');
      for (var i in es) {
        Event.bind(i, this['event_' + i], this, es[i]);
      }
    }
  };

  _model.prototype.classname                = function(){
    return this._classname;
  }

  _model.prototype.init                     = function(){

  };

  _model.prototype.factory                  = function(child_name){
    var key_name                            = '_model/' + (0 < this._classname.length ? this._classname + '/' : '') + child_name.toLowerCase(),
        ancestor                            = this,
        result                              = new Option(),
        src;
    function do_factory(src){
      //console.groupCollapsed(child_name)
      //console.log(src)
      //console.groupEnd()
      Dic.add(key_name, function(){
        var func                            = {},
            model                           = function(){},
            mdl                             = null;
        func[ancestor._classname]           = function(){};
        func[ancestor._classname].prototype = ancestor;
        model.prototype                     = new func[ancestor._classname];
        model.prototype.ancestor            = ancestor;
        model.prototype.constructor         = ancestor.constructor;
        model.prototype.vars                = {VAR_EVENTS:{}};
        model.prototype._streams            = [];
        model.prototype._classname          = child_name;
        model._classname                    = child_name;
        model.handle                        = model.prototype.handle.bind(model.prototype);
        model.invoke                        = model.prototype.invoke.bind(model.prototype);
        model.register                      = model.prototype.register.bind(model.prototype);
        model.module                        = model.prototype.module.bind(model.prototype);
        src(model);
        mdl                                 = new model;
        ancestor.ensure(!(mdl instanceof _model), "The class <" + child_name + "> aren't implements <_model> class.");
        mdl.exception_ensure(mdl.bind_events, mdl);
        mdl.exception_ensure(mdl.init, mdl);
        return mdl;
      });
      result.update({
        done                                : true,
        detail                              : Dic.get(key_name)
      });
    }
    if(!Dic.isset(key_name)){
      if(-1 !== config.models.core.indexOf(child_name)){
        require(['models/' + child_name], do_factory, function(err){
          do_factory(function(){});
        });
      }
      else {
        require(['app/models/' + child_name], do_factory, function(err){
          require(['models/' + child_name], do_factory, function(err){
            do_factory(function(){});
          });
        });
      }
      return result;
    }
    result.update({
      done                                  : true,
      detail                                : Dic.get(key_name)
    });
    return result;
  };

  return _model;
});

define('model', ['_model'], function(_model){
  return new _model;
});

