define('stream', ['option', 'extend'], function(Option, extend){

  function Stream(model, key, alias){
    Option.call(this, model.get(key));
    this._model                                 = model;
    this._offset                                = key;
    this._index                                 = false;
    this.alias(alias);
  }

  extend(Stream, Option);

  Stream.prototype._model                       = null;
  Stream.prototype._offset                      = null;
  Stream.prototype._alias                       = null;

  Stream.prototype.recv                         = function(socket){
    var data                                    = socket.get('VAR_DATA'),
        name                                    = this._alias,
        model                                   = this._model,
        key                                     = this._offset,
        stream;
    if(!data[name]){
      data[name]                                = [];
    }
    this._index                                 = data[name].length;
    data[name].push({
      'model'                                   : model,
      'key'                                     : key,
      'stream'                                  : false
    });
    return this;
  };

  Stream.prototype.send                         = function(socket){
    var name                                    = this._alias,
        stream,
        data                                    = socket.get('VAR_DATA'),
        name                                    = this._alias;
    stream                                      = this
      .then(function(data){
        socket.pack([name, data]);
        return data;
      })
      .nocommit();
    if(!data[name]){
      data[name]                                = [];
    }
    this._index                                 = data[name].length;
    data[name].push({
      'model'                                   : false,
      'key'                                     : false,
      'stream'                                  : stream
    });
    return this;
  };

  Stream.prototype.relate                       = function(socket){
    var data                                    = socket.get('VAR_DATA'),
        name                                    = this._alias,
        model                                   = this._model,
        key                                     = this._offset,
        stream;
    stream                                      = this
      .then(function(data){
        socket.pack([name, data]);
      })
      .nocommit();
    if(!data[name]){
      data[name]                                = [];
    }
    this._index                                 = data[name].length;
    data[name].push({
      'model'                                   : model,
      'key'                                     : key,
      'stream'                                  : stream
    });
    return this;
  };

  Stream.prototype.derelate                     = function(socket){
    var data                                    = socket.get('VAR_DATA'),
        name                                    = this._alias;
    if(false === this._index || !data[name]) return this;
    data[name][this._index].stream.destroy();
    data[name][this._index]                     = null;
    delete data[name][this._index];
    return this;
  };

  Stream.prototype.alias                        = function(alias){
    this._alias                                 = alias ? alias : this._offset;
    return this;
  };

  return Stream;

});
