/**
 * Class: Event
 *
 * @package Event
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
define('event', ['option'], function(Option){

  function ksort(w) {
    var sArr                                = [],
        tArr                                = [],
        n                                   = 0,
        ret                                 = {},
        i;
    for (i in w){
      tArr[n++]                             = i;
    }
    tArr                                    = tArr.sort();
    for(i in tArr){
      ret[tArr[i]] = w[tArr[i]];
    }

    return ret;
  }

  function _event(){
  
  }

  //class Event

  _event.prototype.low_priority             = 100;
  _event.prototype.normal_priority          = 50;
  _event.prototype.high_priority            = 10;
  _event.prototype.express_priority         = 0;
  _event.prototype._events                  = {};
  _event.prototype._executed                = {};

  _event.prototype.is_executed              = function(key){
    var refex                               = null,
        index                               = null,
        reg                                 = new RegExp('^'+key+'$', 'g');
    for(index in this._executed){
      if(reg.test(index) && 'object' === typeof this._executed[index]) return this._executed[index]; 
    }
    return false;
  };

  _event.prototype.bind                     = function(key, callback, context, priority){
    var ret                                 = false,
        event                               = null,
        temp                                = null,
        _this                               = this;
    
    if ('undefined' == typeof priority) priority = this.normal_priority;
    if ('string' != typeof key || 'function'  != typeof callback || !context)  return ret;
    if (event = this.is_executed(key)){
      temp                                  = [callback,context].concat(event.arguments);
      event.option.listen(new Option(function(){
        context.exception_ensure.apply(context, temp);
        return [context].concat(event.arguments);
      }));
      event.option.update(key);
    }
    ret                                     = this.do_bind(key, callback, context, priority);
    return ret;
  };

  _event.prototype.do_bind                  = function(key, callback, context, priority){
    if ('undefined' == typeof  this._events[key]){
       this._events[key]                    = {};
    }
    if ('undefined' != typeof  this._events[key][priority]){
      len                                   =  this._events[key][priority].length;
      this._events[key][priority][len]      = [callback,context];
    }
    else{
      this._events[key][priority]           = [[callback,context]];
    }
    this._events[key]                       = ksort(this._events[key]);
    return true;
  };

  _event.prototype.trigger                  = function(key){
    var k                                   = key,
        err                                 = '',
        starter                             = null,
        current                             = new Option(),
        _this                               = this,
        arg                                 = arguments,
        temp                                = null,
        cb                                  = null,
        reg                                 = null,
        count                               = 0,
        task;
    arg                                     = Array.prototype.slice.call(arg);
    arg.shift();
    this._executed[k]                       = {
      'option'                              : current,
      'arguments'                           : arg
    };
    for(var l in _this._events){
      reg                                   = new RegExp('^'+l+'$');
      if(!reg.test(k)) continue;
      for(var i in this._events[l]){
        for(var j in this._events[l][i]){
          cb                                = this._events[l][i][j];
          temp                              = [cb[0],cb[1]].concat(arg);
          task                              = new Option();
          current.listen(task);
          task.update({done: true, detail: function(){
            cb[1].exception_ensure.apply(cb[1], temp);
            return [cb[1]].concat(arg);
          }});
        }
      }
    }
    current.update({
      done                                  : true,
      detail                                : k
    });
    return current;
  };

  return new _event;
});

