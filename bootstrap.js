define(['module', 'configCore'], function(module, config){
  var path                            = module.uri.split('/'),
      require_core,
      data                            = config.macros ? config.macros : [],
      index                           = 0,
      len                             = 0,
      uri,
      temp,
      paths                           = config.paths ? config.paths : {};
  path.splice(-1, 1);
  path                                = path.join('/');
  
  //paths['text']                       = '/';
  require_core                        = requirejs.config({
    baseUrl                           : path,
    'paths'                           : paths,
    enforceDefine                     : true
  });

  if(!config.models){
    config.models                     = {};
  }

  if(!config.models.core){
    config.models.core                = [
      'action/init/router',
      'action/controller',
      'render/view',
      'socket',
      'cookie',
      'heredoc',
      'polymorph',
      'response',
      'utf8',
      'request',
      'router',
      'node/dependency',
      'render/dom',
      'render/node',
      'render/src',
      'html',
      'node/item'
    ];
  }

  if(!config.controllers){
    config.controllers                = {};
  }

  if(!config.controllers.core){
    config.controllers.core            = [
      'loader'
    ];
  }

  if(!config.controllers.excludeSearch){
    config.controllers.excludeSearch  = [];
  }

  if(!config.views){
    config.views                      = {};
  }

  if(!config.views.core){
    config.views.core                 = [
      'loader'
    ];
  }

  require_core(['event', 'dic', 'option', 'model', 'controller', 'view'], function(){
    require_core(['ready']);
  });
});
