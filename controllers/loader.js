define('controllers/loader', ['model'], function(Model){
  return function(controller){
    controller.prototype._template          = 'loader'

    controller.prototype.action             = function(){
      var el                                = document.createElement('div'),
          view                              = this._view

      document.body.appendChild(el)
      view.set('VAR_ELEMENT', el)

      return Model.register('Node/Dependency', el, 'loader')
      .then(function(dep){
        return Model.register('Render/Node', dep)
      })
      .then(function(node){
        return node.render(view)
      })
      .listen(Model.register('Render/Dom', el))
      .then(function(data, dom){
        dom.render(data)
        return view
      })
    }
  }
})
