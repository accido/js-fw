define('models/action/init/router', function(){
  return function(model){
    model.prototype.done                    = function(ctrl){
      ctrl.commit()
        .listen(model.register('Request').then(function(request){
          request.init_global();
          return request.promise('VAR_REQUEST_URI');
        }))
        .listen(model.register('Router'))
        .listen(model.register('Request'))
        .then(function(){
          var args                          = Array.prototype.slice.call(arguments),
              ctrl                          = args.shift(),
              request                       = args.pop(),
              router                        = args.pop(),
              uri                           = args.pop();
          return router.run(request);
        });
      return ctrl;
    }
}
})
