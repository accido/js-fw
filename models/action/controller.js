define('models/action/controller', function(){
  return function(model){
    model.invoke(function(ctrl){
      ctrl.stream.commit().then(function(ctrl, file){
        return ctrl._action();
      });
      return ctrl;
    });
  }
})
