define('models/router', ['controller', 'option', 'model'],  function(Controller, Option, Model){
  return function(model){
    var base                                      = null;

    model.prototype.vars                          = {
      VAR_EVENTS                                  : {},
      VAR_MODEL_ROUTER                            : null,
      VAR_MODEL_REQUEST                           : null,
      VAR_RUNNED                                  : false,
      VAR_CURRENT_URI                             : null,
      VAR_CURRENT_CONTROLLER_ID                   : 0,
      VAR_CURRENT_CONTROLLER_NAME                 : ''
    };

    model.prototype.init                          = function(){
      base                                        = this;
      base.add('VAR_MODEL_ROUTER', function(){
        return Model.factory('Router');
      });
    };

    model.prototype.execute                       = function(uri, params){
      var result                                  = new Option();
      if('undefined' === typeof params) params    = new Array;

      base.get('VAR_MODEL_ROUTER').then(function(router){
        router.run(uri, params).join(result);
      });

      return result;
    };

    model.prototype.run                           = function(uri, params, view){
      if('undefined' === typeof params) params    = new Array;
      var controller_name                         = base.resolve_controller(uri, params),
          result                                  = new Option();
      base.promise('VAR_MODEL_REQUEST')
        .then(function(request){
            base.set('VAR_RUNNED', true);
            (('undefined' == typeof view || !(view instanceof View.prototype.constructor)) ?
                base.exception_ensure(Controller.execute, Controller, controller_name, params, request) :
                base.exception_ensure(Controller.extend, Controller, controller_name, view, params, request))
              .join(result);
          });

      return result;
    };

    model.prototype.extend                        = function(uri, view, params){
      if (!base.get('VAR_RUNNED')) throw Error('Current controller for this router may be yet executed.');
      var result                                  = new Option();
      if('undefined' === typeof params) params    = new Array;

      base.get('VAR_MODEL_ROUTER').then(function(router){
          router.set('VAR_CURRENT_CONTROLLER_ID', base.get('VAR_CURRENT_CONTROLLER_ID'));
          router.set('VAR_CURRENT_URI', base.get('VAR_CURRENT_URI'));
          router.set('VAR_RUNNED', true);
          router.run(uri, params, view).join(result);
        });

      return result;
    };

    model.prototype.resolve_controller            = function(uri, params){
      uri                                         = base.sanitize_uri(uri);
      var cn                                      = '',
          gl                                      = null,
          id                                      = base.get('VAR_CURRENT_CONTROLLER_ID'),
          l                                       = uri.length,
          param_id                                = 0,
          name                                    = '',
          value                                   = '',
          runned                                  = base.get('VAR_RUNNED');
      if(!runned){
        base.set('VAR_CURRENT_URI', uri);
      }
      var gl                                      = base.get('VAR_CURRENT_URI');
      for(var i = 0; i <= id; i++){
        base.ensure('undefined' === typeof gl[i], 'Action ['+i+']['+uri+'] not defined in request '+gl);
        if(0 !== gl[i].length){
          cn                                      += (cn.length ? '/' : '') + gl[i];
        }
      }
      id++;
      if(false === runned){
        cn                                        += '/main';
      }
      else{
        cn                                        += '/' + uri[0];
      }
      cn                                          = base.sanitize_controller(cn);
      param_id                                    = id;
      for(var i = 1; i < l; i++){
        if('undefined' !== typeof gl[param_id]){  
          name                                    = uri[i].replace(/[^\w].*$/, '');
          value                                   = gl[param_id++].replace(/^[\w]*[^\w]?/, '');
          params[name]                            = value;
        }
        else break;
      }
      base.set('VAR_CURRENT_CONTROLLER_ID', id);
      base.set('VAR_CURRENT_CONTROLLER_NAME', cn);
      return cn;
    };

    model.prototype.sanitize_controller           = function(name){
      return name.replace(/([^\w]|_)+/g, '/').replace(/^\//g, '').toLowerCase();
    };

    model.prototype.sanitize_uri                  = function(uri){
      var current                                 = uri
          , request                               = null;
      if(current instanceof model.prototype.constructor && 'request' === current._classname.toLowerCase()){
        base.set('VAR_MODEL_REQUEST', current);
        request                                   = current;
        current                                   = current.get('VAR_REQUEST_URI');
        request.sleep('VAR_REQUEST_URI');
      }
      else{
        Model.factory('Request').then(function(request){
          base.set('VAR_MODEL_REQUEST', request);
        });
      }
      if(current instanceof Array){
        current                                   = current.join('/');
      }
      
      current                                     = decodeURI(current).toLowerCase();
      current                                     = current.replace(/\?.*$/,'')
        .replace(/\/+/g ,'/')
        .replace(/^\/|\/$/g, '').split('/');
      base.ensure(0 === current.length, 'Uri not found in ' + uri);
      base.promise('VAR_MODEL_REQUEST').then(function(request){
        request.set('VAR_REQUEST_URI', current);
      });
      return current;
    };
  }
})

