define('models/utf8',  function(){
  return function(model){
    var base                                      = null;

    model.prototype.vars                          = {
      VAR_EVENTS                                  : {

      }
    };

    model.prototype.init                          = function(){
      base                                        = this;
    };

    model.prototype.encode_utf8                   = function(s){
      return unescape(encodeURIComponent(s));
    };

    model.prototype.decode_utf8                   = function(s){
      return decodeURIComponent(escape(s));
    };
  }
})
