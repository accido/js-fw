define('models/heredoc', function(){
  return function(model){
    var base                                          = null;

    model.prototype.vars                              = {
      VAR_EVENTS                                      : {

      }
    };

    model.prototype.init                              = function(){
      base                                            = this;
    };

    model.prototype.decode                            = function(code){
      var reg_data                                    = new RegExp('<<<([^\s\r\n\t ]+)((?:(?:.|\n)(?!\\1))*(?:.|\n)?)\\1(?:,|;)?$', 'gm');
          result                                      = '',
          current                                     = null,
          index                                       = 0;
      while(current = reg_data.exec(code)){
        result                                        += code.substr(index, current.index);
        index                                         = current.index + current[0].length;
        result                                        += '(window.' + current[1] +
          ' = (function(){' + base.decode_function(current[2]) + '})(), window.' + current[1] + ')';
      }
      result                                          += code.substr(index);
      return result;
    };

    model.prototype.decode_function                   = function(code){
      var data                                        = code.split(/<%|%>/gm),
      result                                          = "var heredoc_result = '" + base._sanitize_string(data[0]) + "';";
      for(var i = 1; i < data.length; ++i){
        if('=' === data[i].substr(0,1))
          result                                      += 'heredoc_result += ' + data[i] + ';';
        else
          result                                      += data[i];
        if('undefined' !== typeof data[++i]){
          result                                      += "heredoc_result += '" + base._sanitize_string(data[i]) + "';";
        }
      }
      result                                          += 'return heredoc_result;';
      return result;
    };

    model.prototype._sanitize_string                  = function(data){
      return data.replace(/('|\n)/g, "\\$1");
    };
  }
})
