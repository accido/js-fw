define('models/render/src', ['option'], function(Option){
  return function(model){
    var base                                      = null,
        dependency                                = null;

    model.prototype.init                          = function(){
      base                                        = this;
    }

    model.prototype.valid                         = function(){
      return !dependency || dependency.valid();
    }

    model.prototype.build_dependency              = function(){
      return dependency && dependency.build();
    }

    model.prototype.capture                       = function(dic, dep){
      dependency                                  = dep;
      this.ensure(dep && (!(dep instanceof model.prototype.constructor) || ('node/dependency' !== dep.classname())), 'Dependency must be Node/Dependency instance');
    }

    model.prototype.item                          = function(item){
      var src                                     = '',
          i;
      if('string' === typeof item){
        src                                       = item;
      }
      else{
        if(!(item instanceof Array)){
          item                                    = [item];
        }
        for(i in item){
          src                                     +=
            (item[i] instanceof model.prototype.constructor && 'node/item' === item[i].classname()) ?
            item[i].value() : item[i];
        }
      }

      return src;
    }

    model.prototype.render                        = function(view){
      var result                                  = new Option;

      view.render(function(src){
        var reg                                   = new RegExp('\{\{([^}]+)\}\}', 'g'),
            rname                                 = new RegExp('^\{\{([^}]+)\}\}$'),
            expr                                  = src.match(reg),
            index, 
            name,
            stream                                = [],
            listen                                = new Option(1);

        for(index in expr){
          name                                    = expr[index].match(rname)[1];
          stream[stream.length]                   = view.asEventStream(name);
        }

        base.build_dependency();

        listen.all(stream)
        .apply(false, function(item){
          if(!base.valid()){
            listen.destroy();
            for(index in stream){
              stream[index].destroy();
            }
            return;
          }
          var tsrc = src;
          for(index = 0; index < expr.length; ++index){
            tsrc                                 = tsrc.replace(expr[index], base.item(item[index]));
          }
          return base.instance('node/item', tsrc);
        })
        .join(result);
      });

      return result;
    }

    model.invoke(function(ctrl){
      ctrl.stream.commit().then(function(ctrl){
        return base.render(ctrl._view);
      });
      return ctrl;
    });
  }
})

