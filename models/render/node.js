define('models/render/node', ['option'], function(Option){
  return function(model){
    var base                                      = null,
        html                                      = null,
        TYPE_NODE                                 = 1,
        TYPE_ATTRIBUTE                            = 2,
        dependency                                = null;

    model.prototype.init                          = function(){
      base                                        = this;
      html                                        = base.register('Html');
    }

    model.prototype.capture                       = function(dic, dep){
      dependency                                  = dep;
      this.ensure(dep && (!(dep instanceof model.prototype.constructor) || ('node/dependency' !== dep.classname())), 'Dependency must be Node/Dependency instance');
    }

    model.prototype.valid                         = function(){
      return !dependency || dependency.valid();
    }

    model.prototype.build_dependency              = function(){
      return dependency && dependency.build();
    }

    model.prototype.find                          = function(node, name){
      var i,temp,
          child                                   = Array.prototype.slice.call(node.childNodes || []),
          attr                                    = Array.prototype.slice.call(node.attributes || []),
          rname                                   = new RegExp(name),
          param                                   = name.match(/^\{\{([^}]+)\}\}$/)[1];
          
      if(document.TEXT_NODE === node.nodeType){
        if(node.nodeValue.match(rname)){
          return {
            'type'                                : TYPE_NODE,
            'element'                             : [node],
            'root'                                : node.parentNode,
            'param'                               : param
          }
        }
      }

      for(i in attr){
        if(attr[i].nodeValue.match(rname)){
          return {
            'type'                                : TYPE_ATTRIBUTE,
            'param'                               : param,
            'element'                             : {
              'node'                              : node,
              'attr'                              : attr[i].nodeName
            }
          };
        }
      }

      for(i in child){
        if(false !== (temp = base.find(child[i], name))){
          return temp;
        }
      }

      return false;
    }

    model.prototype.prepare                       = function(data, expr){
      var result                                  = [],
          i,j;
      for(i in expr){
        for(j in data){
          if(false !== (result[i] = base.find(data[j], expr[i]))){
            break;
          }
        }
      }

      return result;
    }

    model.prototype.item                          = function(item){
      var data, i;
      if('object' !== typeof item || null === item){
        data                                      = item;
      }
      else{
        if(!(item instanceof Array)){
          item                                    = [item];
        }
        data                                      = [];
        for(i in item){
          data                                    = data.concat(
            (!(item[i] instanceof model.prototype.constructor) || 'node/item' !== item[i].classname()) ?
            item[i] instanceof Array ? item[i] : [item[i]] : item[i].value()
          );
        }
      }

      return data;
    }

    model.prototype.replace                       = function(rule, item){
      var i, data, old, clone;
      try{
      if(!rule){
        return;
      }
      data                                        = base.item(item);

      if(TYPE_NODE === rule.type){
        if(data instanceof Array){
          old                                     = rule.element;
          rule.element                            = [];
          for(i in data){
            clone                                 = data[i].cloneNode(true);
            rule.root.insertBefore(clone, old[0]);
            rule.element.push(clone);
          }
          for(i in old){
            rule.root.removeChild(old[i]);
          }
          return;
        }
        old                                       = rule.element;
        rule.element                              = [document.createTextNode(data)];
        rule.root.insertBefore(rule.element[0], old[0]);
        for(i in old){
          rule.root.removeChild(old[i]);
        }
        return;
      }

      rule.element.node.setAttribute(rule.element.attr, data);
      }
      catch(error){
        console.log(rule.element, error);
      }
    }

    model.prototype.bind                          = function(data, src, view, strm){
      var reg                                     = /\{\{([^}]+)\}\}/g,
          rname                                   = /^\{\{([^}]+)\}\}$/,
          expr                                    = src.match(reg) || [],
          index,
          name,
          stream                                  = [],
          prepared                                = base.prepare(data, expr),
          listen                                  = new Option(1),
          params                                  = [];

      base.build_dependency();

      if(strm){
        strm.then(function(item){
          if(!base.valid()){
            strm.destroy();
            return;
          }
          for(index in prepared){
            base.replace(prepared[index], item[prepared[index].param])
          }
        })
      }
      else{
        for(index in expr){
          name                                      = expr[index].match(rname)[1];
          if(-1 === params.indexOf(name)){
            params.push(name)
            stream.push(view.asEventStream(name));
          }
        }

        listen.all(stream)
        .apply(false, function(item){
          if(!base.valid()){
            listen.destroy();
            for(index in stream){
              stream[index].destroy();
            }
            return;
          }
          for(index in prepared){
            base.replace(prepared[index], item[params.indexOf(prepared[index].param)])
          }
        });
      }

      return base.instance('node/item', data);
    }

    model.prototype.render                        = function(view, stream){
      var result                                  = new Option;

      view.render(function(src){
        html.then(function(model){
          var data                                = model.parse(src);
          return base.bind(data, src, view, stream);
        })
        .join(result);
      });

      return result;
    }

    model.invoke(function(ctrl){
      ctrl.stream.commit().then(function(ctrl, stream){
        return base.render(ctrl._view, stream);
      });
      return ctrl;
    });
  }
})
