define('models/render/dom', function(){
  return function(model){
    var base                                      = null;

    model.prototype.vars                          = {
      VAR_ELEMENT                                 : null,
      VAR_DOM                                     : null,
    };

    model.prototype.init                          = function(){
      base                                        = this;
    }

    model.prototype.capture                       = function(dic, el){
      base.set('VAR_ELEMENT', el);
    }

    model.prototype.search                        = function(sel){
      return base.get('VAR_ELEMENT').querySelector(sel);
    }

    model.prototype.search_all                    = function(sel){
      return base.get('VAR_ELEMENT').querySelectorAll(sel);
    }

    model.prototype.render                        = function(newel){
      var el                                      = base.get('VAR_ELEMENT'),
          newel                                   = (newel instanceof Array ? newel : [newel]),
          index, node, jndex, temp, fragment, i,
          is_source                               = false,
          src                                     = '';

      el.innerHTML                                = '';
      for(index in newel){
        base.ensure(!(newel[index] instanceof model.prototype.constructor) || 'node/item' !== newel[index].classname(), 'Accepted only objects of class Node\\Item');
        temp                                      = newel[index].value();
        if('string' === typeof temp){
          src                                     += temp;
          is_source                               = true;
        }
        else{
          fragment                                = fragment || document.createDocumentFragment();
          for(i in temp){
            fragment.appendChild(temp[i]);
          }
        }
      }

      if(is_source){
        el.innerHTML                              = src;
      }
      else{
        el.appendChild(fragment);
      }
      base.set('VAR_DOM', newel);
    }

    model.invoke(function(ctrl){
      ctrl.stream.commit().then(function(el){
        base.render(el);
        return el;
      });
      return ctrl;
    });
  }
})
