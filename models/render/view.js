define('models/render/view', function(){
  return function(model){
    model.invoke(function(ctrl){
      ctrl.stream.commit().then(function(ctrl){
        return ctrl._view.render();
      });
      return ctrl;
    });
  }
})
