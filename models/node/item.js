define('models/node/item', function(){
  return function(model){
    var base                                      = null;

    model.prototype.vars                          = {
      VAR_VALUE                                   : null,
    };

    model.prototype.init                          = function(){
      base                                        = this;
    }

    model.prototype.capture                       = function(dic, value){
      base.set('VAR_VALUE', value);
    }

    model.prototype.value                         = function(){
      return base.get('VAR_VALUE');
    }
  }
})
