define('models/node/dependency', function(){
  return function(model){
    model.prototype.vars              = {
      VAR_ELEMENT                     : null,
      VAR_ID                          : '',
      VAR_NAME                        : '',
    }

    model.prototype.capture           = function(dic, el, name){
      this.set('VAR_ELEMENT', el);
      this.set('VAR_NAME', name);
      this.ensure(document.ELEMENT_NODE !== el.nodeType, 'Only elements may be dependencies');
    }

    model.prototype.valid             = function(){
      return this.get('VAR_ELEMENT').getAttribute('data-dependency') === this.get('VAR_ID');
    }

    model.prototype.build             = function(){
      var id = new Date().getTime().toString() + Math.floor(Math.random() * 1000).toString();
      this.get('VAR_ELEMENT').setAttribute('data-dependency', id);
      this.set('VAR_ID', id);
    }
  }
})
