define('models/socket', ['controller'], function(Controller){
  return function(model){
    var base                                  = null,
        sync                                  = 0;

    model.prototype.vars                      = {
      VAR_EVENTS                              : {},
      VAR_DATA                                : {},
      VAR_RESOURCE                            : null,
      VAR_ID                                  : null,
      VAR_INDEX                               : null
    };

    model.prototype.init                      = function(){
      var data                                = this.get('VAR_DATA');
      base                                    = this;
      data['x-socket-id']                     = [{
        'model'                               : this,
        'key'                                 : 'VAR_ID'
      }];
      base.set('VAR_DATA', data);

      Controller.execute('loader').then(function(ctrl){
        ctrl.action()
        .listen(base.register_model('Cookie'))
        .then(function(view, cookie){
          //base.set('VAR_ID', cookie.get_cookie('x-socket-id'));
          base.connect();
          base.asEventStream('VAR_ID').then(function(id){
            console.log('cookie', id)
            cookie.set_cookie('x-socket-id', id, {
              expires                             : 60*60
            });
            view.get('VAR_ELEMENT').innerHTML = '';
          });
        });
      });
    };

    model.prototype.connect                   = function(){
      var res                                 = new XMLHttpRequest();
      this.set('VAR_RESOURCE', res);
      this.set('VAR_INDEX', 0);
      this.do_send(res, 'GET', null, this.onready);
    };

    model.prototype.send                      = function(data){
      base.do_send(new XMLHttpRequest(), 'POST', data);
    };

    model.prototype.do_send                   = function(res, method, data, onready){
      var id                                  = base.get('VAR_ID'),
          uri                                 = id ? '/' + id : '';
      if(!method){
        method                                = 'GET';
      }
      uri                                     += ('GET' === method ? '/get' : '/set');
      res.open(method, uri, true);
      res.setRequestHeader('X-Socket', 'application/vnd+relate+json');
      res.setRequestHeader('X-Socket-Id', id);
      res.onreadystatechange                  = onready;
      if(!data){
        data                                  = "";
      }
      else{
        data                                  = JSON.stringify(data);
      }
      res.send(data);
    };

    model.prototype.onready                   = function(){
      var res                                 = base.get('VAR_RESOURCE'),
          data,
          index                               = base.get('VAR_INDEX'),
          len;
      if(3 === res.readyState && 200 === res.status){
        data                                  = res.responseText.split("\n~~#~~\n");
        for(len = data.length - 1; index < len; ++index){
          base.unpack(data[index]);
        }
        base.set('VAR_INDEX', index);
        return;
      }
      if(3 <= res.readyState && 200 !== res.status){
        //disconnected, try reload a page
        return;
      }
    };

    model.prototype.pack                      = function(update){
      base.send(update);
      return true;
    };

    model.prototype.unpack                    = function(response){
      var data                                = base.get('VAR_DATA'),
          find                                = false,
          model, key, stream, index;
      if(!response){
        return false;
      }
      response                                = JSON.parse(response);
      if(!response || !response[0] || !data[response[0]]){
        return false;
      }
      for(index in data[response[0]]){
        row                                   = data[response[0]][index];
        model                                 = row.model;
        key                                   = row.key;
        if(!model){
          continue;
        }
        find                                  = true;
        model.set(key, response[1]);
      }
      return find;
    };
  }
})
