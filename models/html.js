define('models/html', function(){
  return function(model){
    var TAG_NAME                = /<([\w:]+)/,
        TAG_REG                 = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        TAG_WRAP                = {
          option: [ 1, "<select multiple='multiple'>", "</select>" ],
          thead: [ 1, "<table>", "</table>" ],
          col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
          tr: [ 2, "<table><tbody>", "</tbody></table>" ],
          td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],
          _default: [ 0, "", "" ]
        };

    model.prototype.parse       = function(elem){
      var tag, wrap, i,
        fragment = document.createDocumentFragment(),
        nodes,
        tmp = fragment.appendChild(document.createElement("div"));

        tag = (TAG_NAME.exec(elem) || ["", ""])[1].toLowerCase();
        wrap = TAG_WRAP[tag] || TAG_WRAP._default;
        tmp.innerHTML = wrap[1] + elem.replace(TAG_REG, "<$1></$2>") + wrap[2];
        i = wrap[0];
        while(i--){
          tmp = tmp.lastChild;
        }
        nodes = Array.prototype.slice.call(tmp.childNodes);
        fragment.removeChild(fragment.firstChild);

        for(i in nodes){
          fragment.appendChild(nodes[i]);
        }

        return Array.prototype.slice.call(fragment.childNodes);
    }
  }
})
