/**
 * Ex.
 * main.js
 * ...
 * ...
 * .then(register Polymorph('Converter/XML'))
 * ...
 * converter/xml.js
 * capture(injector, param1){
 *  switch(param1){
 *    case bla:
 *    this.done = this.done1
 *    this.fail = this.fail1
 *    case bla-bla:
 *    this.done = this.done2
 *    this.fail = this.fail2
 *    ...
 *  }
 *  var args = [].prototype.slice.call(argumnets);
 *  args.shift();
 *  this.set('bla-bls-bls', args);
 * }
 * .... very long bla-bla-bla .....
 */

define('models/polymorph', function(){
  return function(model){
    var base                                      = null;

    model.prototype.vars                          = {
      VAR_EVENTS                                  : {},
      VAR_NAME                                    : ''
    }

    model.prototype.capture                       = function(injector, name){
      base.ensure('string' != typeof name || !name, 'Polymorph: name is wrong or missed.');
      base.set('VAR_NAME', name);
    };

    model.prototype.init                          = function(){
      base                                        = this;
    };

    model.invoke(function(){
      var args                                    = Array.prototype.slice.call(arguments);
      args.splice(0, 0, base.get('VAR_NAME'));
      return base.register.apply(base, args).then(function(model){
        return model.done();
      });
    }, function(){
      var args                                    = Array.prototype.slice.call(arguments);
      args.splice(0, 0, base.get('VAR_NAME'));
      return base.register.apply(base, args).then(function(model){
        return model.fail();
      });
    });
  }
})
