define('models/cookie', function(){
  return function(model){
    var base                                  = null;

    model.prototype.vars                      = {
      VAR_EVENTS                              : {},
      VAR_COOKIE                              : {},
    };

    model.prototype.init                      = function(){
      var reserved                            = [
          'expires', 'path', 'domain', 'secure', 'httponly', 'max-age'
        ],
        data                                  = document.cookie.toLowerCase().split('; '),
        cookie                                = {},
        item, part;
      base                                    = this;
      for(part in data){
        item                                  = data[part].split('=');
        if(-1 === reserved.indexOf(item[0]) && 'undefined' != typeof item[1]){
          cookie[item[0]]                     = decodeURIComponent(item[1]);
        }
      }
      base.set('VAR_COOKIE', cookie);
    };

    model.prototype.get_cookie                = function(name){
      return base.get('VAR_COOKIE')[name];
    };

    model.prototype.set_cookie                = function(name, value, options){
      var expires, d, cookie, prop, pval;
      base.get('VAR_COOKIE')[name]             = value;

      options = options || {};

      expires = options.expires;
      if ('number' == typeof expires && expires){
        d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
      }
      if (expires && expires.toUTCString){
        options.expires = expires.toUTCString();
      }

      value = encodeURIComponent(value);
      cookie = name + "=" + value;
      for(prop in options){
        cookie += "; " + prop;
        propv = options[prop];
        if(false === propv){
          continue;
        }
        else if(true !== propv){
          cookie += "=" + propv;
        }
      }
      document.cookie = cookie;
    };
  }
})
