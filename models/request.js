define('models/request', function(){
  return function(model){
    var base                                  = null;

    model.prototype.vars                      = {
      VAR_EVENTS                              : {

      },
      VAR_BASE_URL                            : '',
      VAR_REQUEST_URI                         : null,
      VAR_PARAMETERS                          : [],
      VAR_GET                                 : []
    };

    model.prototype.init                      = function(){
      var uri,
          temp,
          index = 0,
          len;
      base                                    = this;
      require(['configCore', 'module'], function(config, module){
        if(!config.baseUrl){
          config.baseUrl                      = {
            auto                              : true,
            src                               : '/'
          };
        }
        if(window && config.baseUrl.auto){
          uri                                 = window.location.pathname.split('/');
          temp                                = module.uri.split('/');
          config.baseUrl.src                  = window.location.protocol + '//' + window.location.host + '/';
          for(len = temp.length; index < len; ++index){
            if(temp[index] === uri[index]){
              config.baseUrl.src              += uri[index] + '/';
            }
          }
        }
        base.set('VAR_BASE_URL', config.baseUrl.src);
      });
    };

    model.prototype.resolve_uri               = function(){
      base.promise('VAR_BASE_URL').then(function(base_url){
        var reg_url                           = new RegExp(base.regexp_escape(base_url)), 
            self_url                          = base.sanitize_url(decodeURI(window.location.href)),
            uri                               = self_url.replace(reg_url, '').split('?')[0].split('/');
        if(uri[0] === ''){
          uri                                 = ['/'];
        }
        base.set('VAR_REQUEST_URI', uri);
        base.set('VAR_GET', base.parse_params(window.location.search.substring(1)));
      });
    };

    model.prototype.init_global               = function(){
      base.resolve_uri();
    };

    model.prototype.sanitize_url              = function(url){
      return url.replace(/\/[\w_\-\.]+\.php(\/?)/g, '$1');
    };

    model.prototype.parse_params              = function(query){
      var re                                  = /([^&=]+)=?([^&]*)/g,
          params                              = {},
          e                                   = null;
      while(e = re.exec(query)){ 
        var k = base.decode(e[1]), v = base.decode(e[2]);
        if (k.substring(k.length - 2) === '[]'){
          k = k.substring(0, k.length - 2);
          (params[k] || (params[k] = [])).push(v);
        }
        else params[k] = v;
      }
      return params;
    };

    model.prototype.decode                    = function(str){
      var decodeRE                            = /\+/g;
      return decodeURIComponent(str.replace(decodeRE, " "));
    };
  }
})
