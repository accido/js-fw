/**
 * Class: View
 *
 * @package View
 *
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 *
 */
define('_view', ['option', 'dic', 'require', 'stream', 'configCore'], function(Option, Dic, require, Stream, config){
  var rscr                                  = document.querySelector('script[src*="require.js"][data-cache="true"]'),
      storage                               = (rscr ? (window.localStorage || {}) : {}),
      version                               = (rscr ? rscr.src.match(/v=([\w.]*)$/) : null),
      storageKey                            = 'view' + ((version && version[1]) || ''),
      cache                                 = (storage[storageKey] && JSON.parse(storage[storageKey])) || {},
      script                                = [].slice.call(document.querySelectorAll('script[type="text/ajs"][data-view]')),
      source                                = {},
      index;

  for(index in script){
    source[script[index].getAttribute('data-view')] = script[index].innerHTML;
  }

  function setToStorage(key, value){
    cache[key]                              = value;
    storage[storageKey]                     = JSON.stringify(cache);
  }

  function getFromStorage(key){
    if ('undefined' != typeof source[key]){
      return source[key];
    }
    else if('undefined' != typeof cache[key]){
      return cache[key];
    }
    return false;
  }

  function _view(name){
    this.set_filename.call(this.constructor.prototype, name);
  };

  _view.prototype.filename                = '';
  _view.prototype.vars                    = {};
  _view.prototype._streams                = {};
  _view.prototype._removed                = false;

  _view.prototype.set_filename            = function (name){
    this.filename                         = name;
  };

  _view.prototype.get_filename            = function(){
    return this.filename;
  };

  _view.prototype._prepare                = function (key){
    if('undefined' === typeof this._streams[key]){
      this._streams[key]                  = new Stream(this,key);
    }
  };

  _view.prototype._set                    = function (vars, key, value){
    var offset                            = key.split('.');
    if(1 === offset.length){
      vars[offset[0]]                     = value;
      return;
    }
    if('undefined' == typeof vars[offset[0]]){
      vars[offset[0]]                     = {};
    }
    this._set(vars[offset[0]], offset.slice(1).join('.'), value);
  };

  _view.prototype._get                    = function (vars, key){
    var offset                            = key.split('.');
    if('undefined' == typeof vars[offset[0]]){
      return undefined;
    }
    if(1 === offset.length){
      return vars[offset[0]];
    }
    return this._get(vars[offset[0]], offset.slice(1).join('.'));
  }

  _view.prototype._isset                  = function(vars, key){
    var offset                            = key.split('.');
    if('undefined' == typeof vars[offset[0]]){
      return false;
    }
    if(1 === offset.length){
      return true;
    }
    return this._isset(vars[offset[0]], offset.slice(1).join('.'));
  }

  _view.prototype._unset                  = function(vars, key){
    var offset                            = key.split('.');
    if('undefined' == typeof vars[offset[0]]){
      return false;
    }
    if(1 === offset.length){
      delete vars[offset[0]];
      return true;
    }
    this._unset(vars[offset[0]]. offset.slice(1).join('.'));
  }

  _view.prototype.set                     = function (key, value){
    this._prepare(key);
    this._set(this.vars, key, value);
    this.trigger(key);
  };

  _view.prototype.trigger                 = function (key){
    this._prepare(key);
    this._streams[key].update({
      done                                : true,
      detail                              : this.get(key)
    });
  };

  _view.prototype.get                     = function (key){
    return this._get(this.vars, key);
  };

  _view.prototype.promise                 = function (key){
    this._prepare(key);
    return this._streams[key].promise(function(data, resolve, reject){
      resolve(data);
    });
  };

  _view.prototype.asEventStream           = function(key){
    this._prepare(key);
    return this._streams[key];
  };

  _view.prototype.asPromiseStream         = _view.prototype.asEventStream;

  _view.prototype.asVipStream             = function (key){
    return this.asEventStream(key).queue();
  };

  _view.prototype.isset                   = function(key){
    return this._isset(this.vars, key);
  };

  _view.prototype.unset                   = function(key){
    return this._unset(this.vars, key);
  };

  _view.prototype.render                  = function(func){
    var _this                             = this,
        result                            = new Option(),
        src;
    
    file = this.get_filename();

    function do_render(src, Model, Controller, Option){
      if(false === getFromStorage(file)){
        setToStorage(file, src);
      }
      var handler                         = func || function(src, Model, Controller, Option){
        var F                             = new Function(['view', 'Model', 'Controller', 'Option'], src);
        F(_this, Model, Controller, Option);
      };
      handler(src, Model, Controller, Option);
      result.update({
        done                              : true,
        detail                            : _this
      });
    }

    if(0 < file.length){
      if(false !== (src = getFromStorage(file))){
        require(['model', 'controller', 'option'], function(Model, Controller, Option){
          do_render(src, Model, Controller, Option);
        });
        return result;
      }
      if(-1 !== config.views.core.indexOf(file)){
        require(['text!views/' + file + '.js', 'model', 'controller', 'option'], do_render, function(err){
          do_render('');
        });
      }
      else{
        require(['text!app/views/' + file + '.js', 'model', 'controller', 'option'], do_render, function(err){
          require(['text!views/' + file + '.js', 'model', 'controller', 'option'], do_render, function(err){
            do_render('');
          });
        });
      }
      return result;
    }

    result.update({
      done                                : true,
      detail                              : this
    });

    return result;
  };

  _view.prototype.remove                  = function(){
    this._removed                         = true;
    for(var i in this._streams){
      this._streams[i].destroy();
    }
    this.vars                             = {};
    this._streams                         = {};
  };

  _view.prototype.factory                 = function(name){
    var key_name                          = '_view/' + (0 < this.filename.length ? this.filename + '/' : '') + name;
    var ancestor                          = this;
    if(!Dic.isset(key_name)){
      Dic.add(key_name, function(){
        var Methods                       = function(){},
            Child                         = function(name){
              _view.call(this, name);
            };
        Methods.prototype                 = ancestor;
        Child.prototype                   = new Methods;
        Child.prototype.constructor       = Child;
        Child.prototype.ancestor          = ancestor;
        Child.prototype.vars              = {};
        Child.prototype._streams          = {};
        Child.prototype.filename          = '';
        Child.prototype._removed          = false;
        return new Child(name);
      });
    }
    var result = Dic.get(key_name);
    return result;
  };

  return _view;
});

define('view', ['_view'], function(_view){
  return new _view('');
});

