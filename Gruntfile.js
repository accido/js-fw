module.exports = function (grunt) {
  
  require('time-grunt')(grunt);

  grunt.task.registerTask('model', 'Prepare model for uglifying', function(){
    var fs = require('fs'),
        path = require('path'),
        mkdirp = require("mkdirp"),
        getDirName = path.dirname;

    function walk(currentDirPath, callback) {
      fs.readdirSync(currentDirPath).forEach(function(name) {
        var filePath = path.join(currentDirPath, name),
            stat = fs.statSync(filePath);
        if(stat.isFile() && !filePath.match(/\.js$/)){
          return;
        }
        if (stat.isFile()) {
          callback(filePath, stat);
        } else if (stat.isDirectory()) {
          walk(filePath, callback);
        }
      });
    }

    function writeFile(path, contents){
      var dir = getDirName(path)
      if(!fs.existsSync(dir)){
        mkdirp.sync(getDirName(path))
      }
      fs.writeFileSync(path, contents)
    }

    walk('min/', function(file, stat){
      var data, name = file.replace(/\.js$/, '').replace(/^min\//, 'models/')
      grunt.log.writeln(file)
      data = fs.readFileSync(path.resolve(__dirname, file), 'UTF-8')
      data = "define('" + name + "', function(){\nreturn '" + data.replace(/'/g, '\\\'').replace(/\n/g, "\\\n") + "'\n})"
      writeFile(path.resolve(__dirname, 'pre/' + file), data)
    });
  })

  require('load-grunt-config')(grunt, {
    jitGrunt: true
  });
};
