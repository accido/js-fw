<style>
  .modal{
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    opacity: 0.1;
    background-color: #333;
    display: block;
  }

  .ajax-loader{
    background-image: url(assets/core/img/loader.gif);
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    width: 66px;
    height: 66px;
    margin: auto;
  }
</style>
<div class="modal"></div>
<div class="ajax-loader"></div>
