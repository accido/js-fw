define('optionError', function(){
  function OptionError(reasone){
    this.detail                               = reasone;
  }
  return OptionError;
});

define('extend', function(){
  function extend(child, parent){
    var prot                                    = function(){};
    prot.prototype                              = parent.prototype;
    child.prototype                             = new prot;
  }
  return extend;
});

define('option', ['optionError', 'extend'], function(OptionError, extend){
  function Option (data){
    this._init(data);
  }
  
  Option.prototype._done                        = function(data){
    return data;
  };

  Option.prototype._fail                        = function(reasone){
    throw new OptionError(reasone);
  };

  Option.prototype._init                        = function(data){
    this._callbacks                             = [];
    this._storage                               = null;
    this._sleep                                 = true;
    this._merged                                = [];
    this._ancestor                              = null;
    this._is_commit                             = false;
    this._is_fail                               = false;
    this._no_commit                             = false;
    this._done                                  = null;
    this._resolve_array                         = false;
    this._fail                                  = null;
    if(data){
      this._storage                             = {
        detail                                  : data,
        done                                    : true
      };
      this._sleep                               = false;
    }
  };

  (function(){
    var context                                 = (window ? window : {}),
        BrowserMutationObserver                 = (context.MutationObserver || context.WebKitMutationObserver),
        set_timeout                             = context.setTimeout,
        requestAnimationFrame                   = (context.requestAnimationFrame ||
          context.webkitRequestAnimationFrame || 
          context.mozRequestAnimationFrame    || 
          context.oRequestAnimationFrame      || 
          context.msRequestAnimationFrame),
        node_process                            = ('undefined' !== typeof process ? process.nextTick : null),
        queue                                   = [],
        lock                                    = 0,
        execute                                 = null;

    function trigger_node_process(){
      return function(){
        node_process(do_execute);
      }
    }

    function trigger_animation_frame(){
      return function(){
        requestAnimationFrame.call(window,do_execute);
      }
    }

    function trigger_mutation_observer(){
      var observer                              = new BrowserMutationObserver(do_execute),
          element                               = document.createElement('div');
      observer.observe(element, {attributes: true});
      window.addEventListener('unload', function(){
        observer.disconnect();
        observer = null;
      }, false);
      return function(){
        element.setAttribute('executed', 'executed');
      };
    }

    function trigger_set_timeout(){
      return function(){
        window.setTimeout(do_execute, 1);
      }
    }

    if(node_process){
      execute                                   = trigger_node_process();
    }
    else if(requestAnimationFrame){
      execute                                   = trigger_animation_frame();
    }
    else if(BrowserMutationObserver){
      execute                                   = trigger_mutation_observer();
    }
    else{
      execute                                   = trigger_set_timeout();
    }

    function do_execute(){
      while(lock++){
        //resolve duplicate executed
        lock--;
        do_execute();
      }
      var executed                              = queue,
          index                                 = 0;
      queue                                     = [];
      lock--;
      for(; index < executed.length; ++index){
        executed[index][0].apply(executed[index][1], executed[index][2]);
      }
    }

    Option.prototype.async                      = function(callback, context, args){
      while(lock++){
        //resolve Array.prototype.push to executed
        lock--;
        this.async(callback, context, args);
      }
      queue.push([callback,context,args]);
      lock--;
      execute();
    }; 

  })();

  Option.prototype.resolve_option               = function(callback, ret){
    var _this                                   = this;
    callback.then(function(data){
      ret.call(_this, {
        done                                    : true,
        detail                                  : data
      });
    }, function(reasone){
      ret.call(_this, {
        detail                                  : reasone
      });
    })
    .nocommit();
  };

  Option.prototype.resolve_callable             = function(callback, ret, arg){
    var event                                   = {};
    if(!this._is_fail){
      event.done                                = true;
    }
    try{
      event.detail                              = this._resolve_array ? callback.apply(this, arg) : callback.call(this, arg);
    }
    catch(error){
      if(error instanceof OptionError){
        error                                   = error.detail;
      }
      this._is_fail                             = true;
      event.done                                = false;
      delete event.done;
      event.detail                              = error;
    }
    ret.call(this, event);
  };

  Option.prototype.do_update                    = function(event){
    var index                                   = 0,
        length                                  = this._callbacks.length;
    this._storage                               = event;
    this._sleep                                 = false;
    for(; index < length; ++index){
      this.do_callback(this._callbacks[index].option, event);
    }
  };

  Option.prototype.do_callback                  = function(option, event){
    var data                                    = {detail: event.detail, done: event.done};
    this.async(function(){
      option.update(data);
    });
  };

  Option.prototype.update                       = function(event){
    var data                                    = event.detail,
        done                                    = this._done,
        fail                                    = this._fail,
        _this                                   = this;

    this._is_fail                               = event.done ? false : true;

    if(data instanceof Option){
      data._is_fail                             = data._is_fail || this._is_fail;
      data
        .then(function(){
          var args                              = Array.prototype.slice.call(arguments),
              ctrl                              = args.shift(),
              index                             = 0,
              len                               = args.length,
              position                          = (_this._ancestor && len ? _this._ancestor._merged.length : -1);
          if(0 < position){
            _this._ancestor.listen_array(args);
          }
          _this.update({detail: ctrl, done: true});
          if(0 < position){
            _this._ancestor._merged.splice(position);
          }
        }, function(reasone){
          _this._is_fail                        = true;
          _this.update({detail: reasone});
        })
        .nocommit();
      return;
    }
    else if('function' === typeof data){
      this.resolve_callable(data, this.update);
      return;
    }
    else if(!this._is_fail && done){
      if(done instanceof Option){
        done.then(function(functor){
            _this.resolve_callable(functor.done, _this.do_update, data);
          }, function(functor){
            _this._is_fail                        = true;
            _this.resolve_callbale(functot.fail, _this.do_update, data);
          })
          .nocommit();
        return;
      }
      this.resolve_callable(done, this.do_update, data);
      return;
    }
    else if(this._is_fail && fail){
      if(fail instanceof Option){
        fail.then(function(functor){
            _this.resolve_callable(functor.done, _this.do_update, data);
          }, function(functor){
            _this.resolve_callbale(functot.fail, _this.do_update, data);
          })
          .nocommit();
        return;
      }
      this.resolve_callable(fail, this.do_update, data);
      return;
    }
    this.do_update(event);
  };

  Option.prototype.destroy                      = function(){
    var i, len;
    if(this._ancestor instanceof Option){
      for(i = 0, len = this._ancestor._callbacks.length; i < len; ++i){
        if(this._ancestor._callbacks[i].option === this){
          this._ancestor._callbacks.splice(i, 1);
          break;
        }
      }
      this._ancestor                            = null;
    }
    this.end();
    this._init();
    return this;
  };

  Option.prototype._add_listener                = function(done,faile){
    var len                                     = this._callbacks.length,
        option                                  = new Option(),
        event;
    option._done                                = done;
    option._fail                                = faile;
    option._ancestor                            = this;
    this._callbacks[len]                        = {
      'option'                                  : option
    };
    if(this._sleep) return option;
    event                                       = {
      detail                                    : this._storage.detail,
      done                                      : this._storage.done
    };
    this.async(function(){
      option.update(event);
    }, this);
    return option;
  };

  Option.prototype._do_then                     = function(data, done, faile){
    var completed                               = 0,
        is_fullfilled                           = false,
        i,
        state                                   = [],
        err                                     = 0,
        len                                     = parseInt(this._merged.length) + 1,
        arr                                     = this._merged,
        result                                  = new Option();

    result._resolve_array                       = true;
    result._done                                = done;
    result._fail                                = faile;

    function set(index){
      var failed                                = false,
          filled                                = 0,
          stream                                = arr[index-1];
      if(stream instanceof Option){
        stream.then(function(data){
          state[index]                          = data;
          if(failed){
            err--;
            failed                              = false;
          }
          if(!(filled++)) iterate();
          else trigger();
        }, function(reasone){
          state[index]                          = reasone;
          if(!failed){
            err++;
            failed                              = true;
          }
          if(!(filled++)) iterate();
          else trigger();
        })
        .nocommit();
        return;
      }
      state[index]                              = stream;
      iterate();
    }

    function trigger(){
      if(len !== completed || !is_fullfilled) return;
      if(0 === err){
        result.update({
          done                                  : true,
          detail                                : state
        });
        return;
      }
      result.update({
        detail                                  : state
      });
    }

    function do_ensure(){
      if(0 === err){
        result.update({
          done                                  : true,
          detail                                : state
        });
        is_fullfilled                           = true;
        return;
      }
      result.update({
        detail                                  : state
      });
      is_fullfilled                             = true;
    }

    function iterate(){
      if (len === ++completed){
        do_ensure();
      }
    }

    state[0]                                    = data;
    iterate();
    for(i=1; i<len; ++i){
      set(i);
    }

    return result;
  };

  Option.prototype.then                         = function(done, faile){
    var _this                                   = this,
        result                                  = this._add_listener(function(data){
          if(done instanceof Option || _this._merged.length > 0){
            return _this._do_then(data, done, faile);
          }
          else if('function' === typeof done){
            return done(data);
          }
          else{
            return data;
          }
        }, faile);
    return result;
  };

  Option.prototype.reduce                       = function(start_acc, callback){
    var _this                                   = this,
        init                                    = false,
        result                                  = null,
        stemp                                   = (new Option()).then(function(callback){
            if(callback.done)
              callback                          = callback.done;
            else
              callback                          = callback.event;
            return {
              done:                             function(){
                var accum                       = start_acc,
                    i                           = 1,
                    state                       = Array.prototype.slice.call(arguments),
                    data                        = state[0],
                    len                         = state.length;
                for(i=1; i<len; ++i){
                  accum                         = callback(accum, state[i], data, i);
                }
                if(init){
                  result._merged.splice(-1,1);
                }
                init                            = true;
                result.listen(accum);
                return data;
              }
            };
          }),
        result                                  = this._add_listener(function(data){
          return _this._do_then(data, stemp);
        });
    if(callback instanceof Option){
      stemp.update({detail: callback, done: true});
    }
    else{
      stemp.update({detail : {
        event                                   : callback
      }, done: true});
    }
    return result;
  };

  Option.prototype.iterate                      = function(pos, callback){
    var _this                                   = this,
        init                                    = false,
        last                                    = 0,
        result                                  = null,
        rstemp                                  = new Option,
        stemp                                   = rstemp.then(function(pos, callback){
            if(callback.done)
              callback                          = callback.done;
            else
              callback                          = callback.event;
            return {
              done:                             function(){
                var ctrl,
                    state                       = Array.prototype.slice.call(arguments),
                    len                         = state.length,
                    i                           = 0,
                    p                           = ('number' !== typeof pos ? 0 : pos),
                    cnt                         = parseInt(state[p]),
                    calle                       = ('function' === typeof callback);
                if('NaN' !== cnt.toString()){
                  state[p]                      = [];
                  for(i=0; i<cnt; ++i){
                    state[p][i]                 = i;
                  }
                }
                if(('undefined' !== typeof state[p]['length']) && !(state[p] instanceof Array)){
                  state[p]                      = Array.prototype.slice.call(state[p]);
                }
                if(state[p] instanceof Array){
                  if(calle){
                    for(i=0, len=state[p].length; i<  len; ++i){
                      state[p][i]               = callback(state[p][i]);
                    }
                  }
                  state                         = state.splice(0,p).concat(state.splice(0,1).shift()).concat(state);
                }
                ctrl                            = state.shift();
                len                             = state.length;
                if(init){
                  result._merged.splice(-last,last);
                }
                for(i=0; i<len; ++i){
                  result.listen(state[i]);
                }
                init                            = true;
                last                            = len;
                return ctrl;
              }
            };
          }),
        result                                  = this._add_listener(function(data){
          return _this._do_then(data, stemp);
        });
    if(callback instanceof Option){
      rstemp.listen(callback);
    }
    else{
      rstemp.listen({
        event                                   : callback
      });
    }
    stemp.update({detail: pos, done: true});
    return result;
  };

  Option.prototype.queue                        = function(pos, callback){
    var result                                  = new Option();
    result._done                                = callback;
    this.iterate(pos, function(value){
      result.update({
        done                                    : true,
        detail                                  : value
      });
    });
    return result;
  };

  Option.prototype.apply                        = function(range, callback){
    var _this                                   = this,
        init                                    = false,
        last                                    = 0,
        result                                  = null,
        rstemp                                  = new Option(),
        stemp                                   = rstemp.then(function(range, callback){
            if(callback.done)
              callback                          = callback.done;
            else
              callback                          = callback.event;
            return {
              done:                             function(){
                var args                        = new Array,
                    state                       = Array.prototype.slice.call(arguments),
                    len                         = state.length,
                    call                        = callback || state[0],
                    i                           = 0,
                    temp_arr                    = new Array;
                if(!range){
                  range                         = new Array;
                  for(i=1, len=state.length; i<len; ++i){
                    range[range.length]         = i;
                  }
                }
                for(i=0, len=range.length; i<len; ++i){
                  if('undefined' !== typeof state[range[i]]){
                    args[i]                     = state[range[i]];
                    continue;
                  }
                  args[i]                       = null;
                }
                for(i=0, len=state.length; i<len; ++i){
                  if(-1 === range.indexOf(i)){
                    temp_arr[temp_arr.length]   = state[i];
                  }
                }
                len                             = temp_arr.length;
                if(init){
                  result._merged.splice(-last, last);
                }
                for(i=0; i<len; ++i){
                  result.listen(temp_arr[i]);
                }
                init                            = true;
                last                            = len;
                return 'function' !== typeof call ? 'function' !== typeof call.done ? args : call.done.call(_this, args) : call.call(_this, args);
              }
            };
          }),
        result                                  = this._add_listener(function(data){
          return _this._do_then(data, stemp);
        });
    if(callback instanceof Option){
      rstemp.listen(callback);
    }
    else{
      rstemp.listen({
        event                                   : callback
      });
    }
    stemp.update({detail: range, done: true});
    return result;
  };

  Option.prototype.filter                       = function(range, callback){
    var _this                                   = this,
        init                                    = false,
        last                                    = 0,
        result                                  = null,
        rstemp                                  = new Option,
        stemp                                   = rstemp.then(function(range, callback){
            if(callback.done)
              callback                          = callback.done;
            else
              callback                          = callback.event;
            return {
              done:                             function(){
                var ctrl,
                    state                       = Array.prototype.slice.call(arguments),
                    len                         = state.length,
                    i                           = 0,
                    err;
                for(i=0, len=range.length; i<len;++i){
                  if('undefined' !== typeof state[range[i]] && !callback(state[range[i]])){
                    err                         = new Error;
                    err.detail                  = state;
                    throw err;
                  }
                }
                ctrl                            = state.shift();
                len                             = state.length;
                if(init){
                  result._merged.splice(-last, last);
                }
                for(i=0; i<len; ++i){
                  result.listen(state[i]);
                }
                init                            = true;
                last                            = len;
                return ctrl;
              }
            };
          }),
        result                                  = this._add_listener(function(data){
          return _this._do_then(data, stemp);
        });
    if(callback instanceof Option){
      rstemp.listen(callback);
    }
    else{
      rstemp.listen({
        event                                   : callback
      });
    }
    stemp.update({detail: range, done: true});
    return result;
  };

  Option.prototype.join                         = function(stream, callback){
    var root                                    = stream.commit(callback),
        result                                  = root;
    while(root._ancestor instanceof Option){
      root                                      = root._ancestor;
    }
    for(var i=0, len=root._merged.length; i<len; ++i){
      this._merged[this._merged.length]         = root._merged[i];
      root._merged[i]._ancestor                 = this;
    }
    root._ancestor                              = this;
    root._storage                               = null;
    root._sleep                                 = true;
    root._merged                                = this._merged;
    this._callbacks[this._callbacks.length]     = {
      'option'                                  : root
    };
    if(this._sleep) return result;
    root.update({detail: this._storage.detail, done: this._storage.done});
    return result;
  };

  Option.prototype.nocommit                     = function(){
    this._no_commit                             = true;
    return this;
  };

  Option.prototype.commit                       = function(callback){
    var result                                  = {'commit':false,'stream':this},
        len                                     = parseInt(this._callbacks.length),
        i,
        temp,
        strm,
        calle                                   = ('function' === typeof callback);
    if(0 === len){
      return !this._is_commit || (calle && callback(this)) ? this : result;
    }
    for(i=0; i<len; ++i){
      strm                                      = this._callbacks[i].option;
      if(strm._no_commit){
        continue;
      }
      result.stream                             = strm;
      strm._is_commit                           = true;
      temp                                      = strm.commit(callback);
      strm._is_commit                           = false;
      if(temp instanceof Option){
        result.stream                           = temp;
        result.commit                           = true;
        return this._is_commit ? result : temp;
      }
      else if(temp && 'undefined' !== typeof temp['commit'] && temp['commit']){
        return this._is_commit ? temp : temp['stream'];
      }
      else{
        result                                  = temp;
      }
    }
    return this._is_commit ? result : result['stream'];
  };

  Option.prototype.end                          = function(){
    var child                                   = [],
        index                                   = 0,
        len                                     = this._callbacks.length;
    for(; index < len; ++index){
      child[index]                              = this._callbacks[index];
    }
    for(index = 0, len = child.length; index < len; ++index){
      child[index].option.destroy();
    }
    this._callbacks                             = [];
    return this;
  };

  Option.prototype.done                         = function(done){
    return this.then(done, null);
  };

  Option.prototype.fail                         = function(faile){
    return this.then(null, faile);
  };

  Option.prototype.promise                      = function(resolver){
    var result                                  = new Promise(resolver, false, true),
        event;

    this._callbacks[this._callbacks.length]     = {'option': result};
    result._ancestor                            = this;
    if(this._sleep) return result;
    event                                       = {
      detail                                    : this._storage.detail,
      done                                      : this._storage.done
    };
    this.async(function(){
      result.update(event);
    });
    return result;
  };

  Option.prototype.when                         = function(resolver){
    var result                                  = new Promise(resolver),
        event;

    this._callbacks[this._callbacks.length]     = {'option': result};
    result._ancestor                            = this;
    if(this._sleep) return result;
    event                                       = {
      detail                                    : this._storage.detail,
      done                                      : this._storage.done
    };
    this.async(function(){
      result.update(event);
    }); 
    return result;
  };

  Option.prototype.off                          = function(){
    var i, len;
    if(this._ancestor instanceof Option){
      for(i = 0, len = this._ancestor._callbacks.length; i < len; ++i){
        if(this._ancestor._callbacks[i].option === this){
          this._ancestor._callbacks.splice(i, 1);
          break;
        }
      }
    }
    for(i = 0, len = this._callbacks.length; i < len; ++i){
      this._callbacks[i].option._ancestor       = null;
    }
    this._callbacks                             = [];
    this._ancestor                              = null;
    return this;
  };

  Option.prototype.merge                        = function(){
    var result                                  = this._add_listener();
    result.listen.apply(result, Array.prototype.slice.call(arguments));
    return result;
  };

  Option.prototype.merge_array                  = function(stream){
    if (!(stream instanceof Array)) throw new OptionError(['Merge all failed.', stream]);
    return this.merge.apply(this, stream);
  };

  Option.prototype.listen                       = function(){
    var args                                    = Array.prototype.slice.call(arguments),
        len                                     = args.length,
        i;

    for(i=0; i<len; ++i){
      this._merged[this._merged.length]         = args[i];
    }
    return this;
  };

  Option.prototype.listen_array                 = function(stream){
    if (!(stream instanceof Array)) throw new OptionError(['Listen all failed.', stream]);
    return this.listen.apply(this, stream);
  };

  Option.prototype.all                          = Option.prototype.listen_array;

  function Promise(resolver, data, locked){
    Option.call(this, data);
    this.resolve_promise(resolver, locked);
  }
  
  extend(Promise, Option);

  Promise.prototype.do_resolve_promise          = Promise.prototype.do_update;

  Promise.prototype.do_update                   = function(){};

  Promise.prototype.resolve_promise             = function(resolver, locked){
    var lock                                    = 0,
        _this                                   = this;
    function resolve(data){
      if(!(lock++ && locked)){
        _this.do_resolve_promise({detail: data, done: true});
      }
    }
    function reject(reasone){
      if(!(lock++ && locked)){
        _this.do_resolve_promise({detail: reasone});
      }
    }
    this._done                                  = function(data){
      if(lock++ && locked) return;
      lock--;
      resolver(data, resolve, reject);
    };
    this._fail                                  = this._done;
  };

  return Option;
});
