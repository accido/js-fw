module.exports  = {
  options       : {
    spawn       : false,
    livereload  : true,
  },
  scripts       : {
    files       : [
      '*.js',
      'models/**/*.js'
    ],
    tasks       : [
      'jshint',
      'uglify'
    ]
  }
}
