module.exports        = {
  options             : {
    reporter          : require('jshint-stylish')
  },
  main                : [
    'models/*.js', '.*js'
  ]
}
