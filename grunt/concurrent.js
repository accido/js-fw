module.exports      = {
  options           : {
    limit           : 3
  },
  devFirst          : [
    'clean',
    'jshint'
  ],
  devSecond         : [
    'uglify:core'
  ],
  prodFirst         : [
    'clean',
    'jshint',
  ],
  prodSecond        : [
    'uglify:core'
  ]
}
